#!/bin/bash
awk '{ printf("%6.2f %s\n", $2 * $3, $1) }' emp.data | sort
