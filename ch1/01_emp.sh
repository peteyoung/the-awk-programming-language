#!/bin/bash
set -x

# show employees who worked
awk '$3 > 0 { print $1, $2, $3 }' emp.data

# show names of employees who did not work
awk '$3 == 0 { print $1 }' emp.data

set +x
