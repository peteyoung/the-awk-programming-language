# seq - print sequences of integers
#       input:  arguments q, q p, or q p r: q >= p; r > 0
#       output: integers 1 to q, p to q, or p to q in steps of r


# Ex.
# awk -f 21_seq.awk 10
# awk -f 21_seq.awk 5 10
# awk -f 21_seq.awk 2 20 3

BEGIN {
  if (ARGC == 2) {
    for (i = 1; i <= ARGV[1]; i++) {
      print i
    }
  }
  else if (ARGC == 3) {
    for (i = ARGV[1]; i <= ARGV[2]; i++) {
      print i
    }
  }
  else if (ARGC == 4) {
    for (i = ARGV[1]; i <= ARGV[2]; i += ARGV[3]) {
      print i
    }
  }
}
