{ print $0, "max:", max($1, max($2, $3)) }

function max(m, n) {
  return (m > n) ? m : n
}
