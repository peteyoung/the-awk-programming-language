# sum2 - print column sums
#   chech that each line ahs the same number of fields
#     as line one
#
# Ex.
#   awk -f 02_sum2.awk 01_columns
#   awk -f 02_sum2.awk 02_columns

NR==1 { nfld = NF }
      { for (i = 1; i <= NF; i++) {
          sum[i] += $i
          if (NF != nfld) {
            print "line " NR " has " NF " entries. expected " nfld
          }
        }
      }
END   { for (i = 1; i <= nfld; i++) {
          printf("%g%s", sum[i], i < nfld ? "\t" : "\n")
        }
      }
