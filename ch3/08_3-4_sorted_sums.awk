# sorted_sums - sum a list of items and print it out sorted
#
# Ex.
#   awk -f 08_3-4_sorted_sums.awk 08_3-4_list

    { items[$1] += $2 }
END { for (key in items) {
        printf("%-8s%3d\n", key, items[key]) | "sort"
      }
    }
