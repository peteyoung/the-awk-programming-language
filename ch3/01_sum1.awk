# sum1 - print column sums
#   input:  rows of numbers
#   output: sum of each colum
#     missing entries are treated as zeroes
#
# Ex.
#   awk -f 01_sum1.awk 01_columns

    { for (i = 1; i <= NF; i++) {
        sum[i] += $i
        if (NF > maxfld) {
          maxfld = NF
        }
      }
    }
END { for (i = 1; i <= maxfld; i++) {
        printf("%g", sum[i])
        if (i < maxfld) {
          printf("\t")
        }
        else {
          printf("\n")
        }
      }
    }
