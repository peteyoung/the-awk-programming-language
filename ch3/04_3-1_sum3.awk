# sum3 - print sums of numeric columns
#     input:  rows of integers and strings
#     output: sums of numeric columns
#             assumes every line has the same layout
#
#     ignores blank lines
#
# Ex.
#   awk -f 04_3-1_sum3.awk 03_columns

nfld == 0 && NF > 0 {
          nfld = NF
          for (i = 1; i <= NF; i++) {
            numcol[i] = isnum($i)
          }
        }
NF > 0  { for (i = 1; i <= NF; i++) {
            if (numcol[i]) {
              sum[i] += $i
            }
          }
        }
END     { for (i = 1; i <= nfld; i++) {
            if (numcol[i]) {
              printf("%g", sum[i])
            } else {
              printf("--")
            }
            printf(i < nfld ? "\t" : "\n")
          }
        }

function isnum(n) { return n ~ /^[+-]?[0-9]+$/ }
