#!/bin/bash

readonly FILES=($(ls -lh ch1/{*.awk,*.sh} | sort -k 7,7 -k 8,8 | awk '{ print $9 }'))

main() {
	#echo ${#FILES[@]}
	for ((idx=0; idx < ${#FILES[@]}; idx++)); do
		local old_name=${FILES[idx]}
		local split_path=(${old_name//// })
		local dir=${split_path[0]}
		local name=${split_path[1]}
		local num=$((idx+1))
		local padded_num=$(printf %02d $num)

		local new_name=$dir/${padded_num}_$name
		#echo -e "${old_name}\t${new_name}"
		git mv $old_name $new_name
	done
}

main
